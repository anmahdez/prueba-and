export interface Actualidad {
    id: number;
    titulo: string;
    descripcion: string;
    botonTexto: string;
    botonTextoAlternativo: string;
  }
  