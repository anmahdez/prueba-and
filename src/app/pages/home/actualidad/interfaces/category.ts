export interface Category {
    id: number;
    titulo: string;
    sumario: string;
    imagen: string;
    textoAlternativo: string;
    textoFecha: string;
}
