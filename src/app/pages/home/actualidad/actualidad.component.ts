import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { Category } from "./interfaces/category";
import { Actualidad } from "./interfaces/actualidad";
import { ActualidadService } from "./services/actualidad.service";

@Component({
  selector: "app-actualidad",
  templateUrl: "./actualidad.component.html",
  styleUrls: ["./actualidad.component.css"],
})
export class ActualidadComponent implements OnInit, OnDestroy {
  public categories: Category[] = [];
  public actualidad: Actualidad = {} as Actualidad;
  pageSuscription = new Subscription();
  constructor(private actualidadService: ActualidadService) {}

  ngOnInit() {
    this.loadCategories();
    this.loadActualidad();
  }

  loadActualidad() {
    this.pageSuscription.add(
      this.actualidadService.get().subscribe(
        (result) => {
          this.actualidad = result.data;
          console.log(this.actualidad);
        }
      )
    );
  }

  loadCategories() {
    this.pageSuscription.add(
      this.actualidadService.getCategories().subscribe(
        (result) => {
          this.categories = result.data;
          console.log(this.categories);
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.pageSuscription.unsubscribe();
  }
}
