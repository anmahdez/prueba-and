import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { constants } from 'src/app/utils/constants';
import { HelperService } from 'src/app/utils/helper.service';


@Injectable({
  providedIn: 'root'
})
export class ActualidadService {

  constructor(private helper: HelperService) { }

  get() :Observable <any> {
    return this.helper.get(constants.serviceUrls.noticias.actualidad);
  }

  getCategories() :Observable <any>  {
    return this.helper.get(constants.serviceUrls.noticias.categorias);
  }
}
