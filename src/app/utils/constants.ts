export const constants = {
    url_api: "https://api-interno.www.gov.co/api/",
    serviceUrls: {
      home: {
        bannerPrincipal: "home/BannerPrincipal",
        bannerInformativo: "home/BannerInformativo",
      },
      noticias: {
        actualidad: "noticias/secciones/actualidad-general",
        categorias: "noticias/Noticias/categorias/0",
      },
    },
  };