import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
} from "@angular/common/http";
import { throwError, Observable, of, concat } from "rxjs";
import { catchError, retryWhen, concatMap, delay, take } from "rxjs/operators";
import { constants } from "./constants";

@Injectable({
  providedIn: "root",
})
export class HelperService {
  API_URL = constants.url_api;

  constructor(private http: HttpClient) { }

  public get<T>(url: string, params?: HttpParams): Observable<any> {
    const headers = new HttpHeaders().set("Content-Type", "application/json");
    return this.http
      .get<T>(`${this.API_URL}${url}`, { headers, params })
      .pipe(
        retryWhen(
          (
            errors
          ) =>
            errors.pipe(
              concatMap((result) => {
                if (result.status === 504) {
                  return of(result);
                }
                return throwError(result);
              }),
              delay(1000),
              take(4),
              (o) =>
                concat(
                  o,
                  throwError(`No fue posible conectarse con el servidor.`)
                )
            )
        ),
        catchError((err: HttpErrorResponse) => {
          return this.handleError(err);
        })
      );
  }

  handleError(error: HttpErrorResponse) {
    let messageError: string;
    messageError = error.error.message;
    console.log(messageError);
    return throwError(error);
  }
}
