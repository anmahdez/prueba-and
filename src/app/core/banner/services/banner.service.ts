import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HelperService } from 'src/app/utils/helper.service';
import { constants } from 'src/app/utils/constants';

@Injectable({
  providedIn: 'root'
})
export class BannerService {

  constructor(private helper: HelperService) { }

  get(): Observable<any> {
    return this.helper.get(constants.serviceUrls.home.bannerPrincipal);
  }
}
