import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataBanner } from './interface/dataBanner';
import { Subscription } from 'rxjs';
import { BannerService } from './services/banner.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit, OnDestroy {
  public databanner: DataBanner =
    {} as DataBanner;
  public urlImagen: string = "";
  public textoDescriptivo: string = "";
  pageSuscription = new Subscription();

  constructor(private bannerservice: BannerService) { }

  ngOnInit() {
    this.load();
  }

  load() {
    this.pageSuscription.add(
      this.bannerservice.get().subscribe(
        (result) => {
          this.databanner = result.data;
          this.urlImagen =
            this.databanner.listaImagenes[0].urlImagen;
          this.textoDescriptivo =
            this.databanner.listaImagenes[0].textoDescriptivo;
          console.log(this.databanner);
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.pageSuscription.unsubscribe();
  }
}
