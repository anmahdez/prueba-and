import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule} from '@angular/common/http'
import { BannerComponent } from './banner/banner.component';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { TemplateComponent } from './template/template.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    BannerComponent,
    ContentComponent,
    FooterComponent,
    HeaderComponent,
    TemplateComponent
  ], 
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ],
  exports: [
    TemplateComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [HttpClientModule]
})
export class CoreModule { }
